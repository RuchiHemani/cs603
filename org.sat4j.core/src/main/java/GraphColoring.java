
/**
 * This code takes the nodes and asks user whether they want to give the adjacency matrix or want to use the randomly generated adjacency matrix 
 * Based on the user input, it either uses the adjacency matrix given by the user or the one randomly generated
 * and  it generates the clauses in Dimac's format and give it to the sat solver.
 * It also measures the time taken to generate all possible models for a particular number of nodes and adjacency matrix
 */

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.IVecInt;
import org.sat4j.specs.TimeoutException;

public class GraphColoring {

    public static void main(String args[])
            throws ContradictionException, TimeoutException, IOException {

        Scanner scan = new Scanner(System.in);
        Random randomNumberGenerator = new Random();
        System.out.print("Enter number of nodes: ");
        // Getting nodes from the user
        int nodes = scan.nextInt();

        int[][] matrix = new int[nodes][nodes];
        System.out.println(
                "Do you want to use automatically generated matrix? Enter y or n");
        String option = scan.next();
        int highestDegree = 0;
        if (option.equalsIgnoreCase("n")) {
            System.out.println("Enter adjacency matrix row");

            for (int i = 0; i < nodes; i++) {
                int degree = 0;
                for (int j = 0; j < nodes; j++) {
                    // Getting adjacency matrix from the user
                    matrix[i][j] = scan.nextInt();
                    if (matrix[i][j] == 1) {

                        degree++;
                    }
                }
                // Finding highest degree of the graph and using it as the
                // chromatic
                // number
                if (degree > highestDegree) {
                    highestDegree = degree;
                }
            }
        } else {
            System.out.println("Adjacency matrix");
            for (int i = 0; i < nodes; i++) {
                int degree = 0;
                for (int j = 0; j < nodes; j++) {
                    if (randomNumberGenerator.nextBoolean()) {
                        matrix[i][j] = 0;
                    } else {
                        matrix[i][j] = 1;
                    }
                    System.out.print(matrix[i][j] + " ");
                    if (matrix[i][j] == 1) {

                        degree++;
                    }
                }
                System.out.println();
                // Finding highest degree of the graph and using it as the
                // chromatic
                // number
                if (degree > highestDegree) {
                    highestDegree = degree;
                }

            }

        }

        int numberOfColors = highestDegree;
        // Setting the start time
        long startTime = System.currentTimeMillis();

        boolean sat = true;
        boolean hasAtleastOneModel = false;
        while (sat) {

            ISolver solver = SolverFactory.newDefault();

            IVecInt clause = new VecInt();

            // Adding clauses for contraint 1-Each node of the graph is colored
            // by atleast one color
            // Assuming 11 means node 1 colored with color 1, 12 means node 1 is
            // colored with color 2, 21 means node 2 is colored with color 1 and
            // so on.
            // 11\/ 12\/13\/..1k where k=number of colors,
            // 21\/22\/23\/....2k where k=number of colors,
            // .
            // '
            // n1\/n2\/n3\/...nk where n= number of nodes and k= number of
            // colors.
            // Assigning 11->1, 12->2, 13->3, 21->4 and so on.
            // Hence initializing firstClauseLiteral to 1
            int firstClauseLiteral = 1;

            for (int i = 0; i < nodes; i++) {

                for (int j = 0; j < numberOfColors; j++) {
                    clause.push(firstClauseLiteral);

                    firstClauseLiteral++;
                }
                solver.addClause(clause);

                clause.clear();

            }

            // Adding clauses for contraint 2 - No two adjacent nodes are
            // colored by the same color.
            // For eg, ~11\/~21, ~12\/~22,~13\/~23 \/...~1k\/~2k if node 1 is
            // connected with node 2 andso on for all edges

            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    if (i < j && matrix[i][j] == 1) {
                        for (int r = 0; r < numberOfColors; r++) {

                            clause.push(-(1 + (numberOfColors * i) + r))
                                    .push(-((1 + (numberOfColors * i) + r)
                                            + numberOfColors * (j - i)));
                            solver.addClause(clause);

                            clause.clear();

                        }

                    }

                }
            }

            // Adding clauses for constraint 3 - Each node of the graph is
            // not colored by more than one color
            // ~11\/~12, ~11\/~13, ~ 12\/~13 and so on for all the nodes.
            // if there are n nodes, there will be n*(k*2/2) clauses. where k=
            // number of colors
            for (int t = 0; t < nodes; t++) {
                for (int i = 0; i < numberOfColors; i++) {
                    for (int j = 0; j < numberOfColors; j++) {
                        if (j > i) {
                            clause.push(-(i + 1 + (numberOfColors * t)))
                                    .push(-(j + 1 + (numberOfColors * t)));

                            solver.addClause(clause);

                            clause.clear();
                        }

                    }
                }

            }

            IProblem problem = solver;

            if (problem.isSatisfiable()) {
                System.out.println("satisfied");
                hasAtleastOneModel = true;
                System.out.println(
                        "Number of colors considered=" + numberOfColors);

                sat = true;

                int[] itermodel = problem.model();

                System.out.println("Model number 1");
                for (int i = 0; i < itermodel.length; i++) {
                    // Printing the first model
                    if (i % numberOfColors == 0) {
                        System.out.print(
                                "Node " + ((i / numberOfColors) + 1) + "  ");
                    }
                    if (itermodel[i] > 0) {
                        System.out.print("Color " + ((i % numberOfColors) + 1));
                        System.out.println();
                    }

                }
                boolean hasAnotherModel = true;
                int modelNumber = 2;
                while (hasAnotherModel) {
                    // Negating the model achieved in previous loop to get
                    // another model
                    for (int i = 0; i < itermodel.length; i++) {
                        clause.push(-(itermodel[i]));
                    }
                    ISolver anotherModelsolver = solver;
                    anotherModelsolver.addClause(clause);

                    clause.clear();
                    IProblem anotherModelProblem = anotherModelsolver;
                    if (anotherModelProblem.isSatisfiable()) {

                        int[] anotherIterModel = anotherModelProblem.model();
                        System.out.println();
                        System.out.println("Model number " + modelNumber);
                        // Printing other possible models
                        for (int r = 0; r < anotherIterModel.length; r++) {

                            if (r % numberOfColors == 0) {
                                System.out.print("Node "
                                        + ((r / numberOfColors) + 1) + "  ");
                            }
                            if (anotherIterModel[r] > 0) {
                                System.out.print(
                                        "Color " + ((r % numberOfColors) + 1));
                                System.out.println();
                            }

                        }
                        itermodel = anotherIterModel;
                        modelNumber++;

                    } else {
                        hasAnotherModel = false;
                        System.out.println(
                                "There are no more possible models if we consider number of colors = "
                                        + numberOfColors);
                    }
                }
                // Decrementing the numberOfColors to get the chromatic number
                numberOfColors = numberOfColors - 1;

            }

            else if (!problem.isSatisfiable()) {

                sat = false;
                if (hasAtleastOneModel) {
                    System.out.println(
                            "chromatic number=" + (numberOfColors + 1));
                } else {
                    System.out.println("It is an unsat problem");
                }
                break;
            }
        }

        // Setting the end time
        long endTime = System.currentTimeMillis();

        System.out.println("Total time taken in seconds for " + nodes
                + " nodes=  " + (endTime - startTime) + " milliseconds");
    }

}
